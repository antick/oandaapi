﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.OandaApi.Configuration;
using NUnit.Framework;

namespace Antick.OandaApi.Tests
{
    [TestFixture]
    public class IntegrationTests
    {
        private const string TOKEN_DEMO = "01dce839792e5821733a05978443acc8-da12c15923e13d308035995973db7f2b";
        private const string TOKEN_REAL = "2313edf7a4682b2e4ae3086a79280386-a2f5438a8d7a35a82a07f99f51a35c0f";
        
        [Test]
        public void DemoConnection()
        {
            var client = new OandaClient(AccountType.Demo, TOKEN_DEMO);
            var data = client.GetAccountDetails(8262000);
        }

        [Test]
        public void RealConnection()
        {
            var client = new OandaClient(AccountType.Real, TOKEN_REAL);
            var data = client.GetAccountDetails(6170764);
        }

        [Test]
        public void Get1MonthData_ShouldSuccess()
        {
            var client = new OandaClient(AccountType.Demo, TOKEN_DEMO);

            var start = DateTime.UtcNow.AddMonths(-1);
            var end = DateTime.UtcNow;

            var data = client.GetCandlesMid("EUR_USD", "H1", start, end);
            Assert.IsTrue(data != null);
        }

        [Test]
        public void Get15000S1_ShouldSuccess()
        {
            var client = new OandaClient(AccountType.Demo, TOKEN_DEMO);
            
            var data = client.GetCandlesMid("EUR_USD", "S5", 10);
            Assert.IsTrue(data != null);
        }

    }
}

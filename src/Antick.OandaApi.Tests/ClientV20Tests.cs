﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.OandaApi.Configuration;
using NUnit.Framework;

namespace Antick.OandaApi.Tests
{
    public class ClientV20Tests
    {

        [Test]
        public void GetCandleBidAsk()
        {
            var component = new OandaClientV20(AccountType.Demo, "ad903858ba0d16e7fdbc8ae3d9ab58c2-f2ef922fa2625f66edea259dfe230055");

            var data = component.GetCandles("EUR_USD", "S5");
        }


        [Test]
        public void GetCandleMidDefault()
        {
            var component = new OandaClientV20(AccountType.Demo, "ad903858ba0d16e7fdbc8ae3d9ab58c2-f2ef922fa2625f66edea259dfe230055");

            var data = component.GetCandlesMid("EUR_USD", "S5");
        }

        [Test]
        public void GetCandleMidFrom()
        {
            var component = new OandaClientV20(AccountType.Demo, "ad903858ba0d16e7fdbc8ae3d9ab58c2-f2ef922fa2625f66edea259dfe230055");

            var data = component.GetCandlesMid("EUR_USD", "S5", DateTime.Now.AddDays(-1), 10);
        }

        [Test]
        public void GetCandleMidFromTo()
        {
            var component = new OandaClientV20(AccountType.Demo, "ad903858ba0d16e7fdbc8ae3d9ab58c2-f2ef922fa2625f66edea259dfe230055");

            var data = component.GetCandlesMid("EUR_USD", "S5", DateTime.Now.AddDays(-1), DateTime.Now);
        }
    }
}

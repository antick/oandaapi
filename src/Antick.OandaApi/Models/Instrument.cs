﻿namespace Antick.OandaApi.Models
{
    public class Instrument
    {
        public string instrument;
        public string displayName { get; set; }
        public string pip;
        public int pipLocation;
        public int extraPrecision;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.OandaApi.Models
{
    public class CandleMidV20
    {

        public bool Complete { get; set; }
        public int Volume { get; set; }
        public DateTime Time { get; set; }
        public CandleItemV20 Mid { get; set; }

        public double Open => Mid.O;

        public double Close => Mid.C;

        public double High => Mid.H;

        public double Low => Mid.L;
    }
}

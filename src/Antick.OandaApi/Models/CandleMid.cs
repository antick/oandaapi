﻿using System;

namespace Antick.OandaApi.Models
{
    [Serializable]
    public class CandleMid
    {
        public DateTime Time { get; set; }
        public double OpenMid { get; set; }
        public double HighMid { get; set; }
        public double LowMid { get; set; }
        public double CloseMid { get; set; }
        public int Volume { get; set; }
        public bool Complete { get; set; }
    }
}

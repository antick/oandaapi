﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.OandaApi.Models
{
    public class CandleV20
    {
        public bool Complete { get; set; }
        public int Volume { get; set; }
        public DateTime Time { get; set; }
        public CandleItemV20 Bid { get; set; }
        public CandleItemV20 Ask { get; set; }

        public double Open
        {
            get
            {
                var val = new double[] { Bid.O, Ask.O }.Average();
                return val;
            }
        }

        public double Close
        {
            get
            {
                var val = new double[] { Bid.C, Ask.C }.Average();
                return val;
            }
        }

        public double High
        {
            get
            {
                var val = new double[] { Bid.H, Ask.H }.Max();
                return val;
            }
        }

        public double Low
        {
            get
            {
                var val = new double[] { Bid.L, Bid.L }.Min();
                return val;
            }
        }
    }
}

﻿using System;
using System.Linq;

namespace Antick.OandaApi.Models
{
    public class Candle
    {
        public DateTime Time { get; set; }
        public double OpenBid { get; set; }
        public double OpenAsk { get; set; }
        public double HighBid { get; set; }
        public double HighAsk { get; set; }
        public double LowBid { get; set; }
        public double LowAsk { get; set; }
        public double CloseBid { get; set; }
        public double CloseAsk { get; set; }
        public int Volume { get; set; }
        public bool Complete { get; set; }

        public double Open {
            get
            {
                var val = new double[] { OpenBid, OpenAsk }.Average();
                return val;
            }
        }

        public double Close
        {
            get
            {
                var val = new double[] { CloseBid, CloseAsk }.Average();
                return val;
            }
        }

        public double High
        {
            get
            {
                var val = new double[] { HighBid, HighAsk }.Max();
                return val;
            }
        }

        public double Low
        {
            get
            {
                var val = new double[] { LowBid, LowAsk }.Min();
                return val;
            }
        }
    }
}

﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models
{
    public class PricePoint
    {
        public double price { get; set; }
        public List<PricePointItem>  Items { get; set; }
    }
}

﻿namespace Antick.OandaApi.Models
{
    public class Account
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountCurrency { get; set; }
        public double MarginRate { get; set; }
    }
}

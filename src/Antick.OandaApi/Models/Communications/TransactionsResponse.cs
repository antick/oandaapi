﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    public class TransactionsResponse
    {
        public List<Transaction> transactions;
        public string nextPage;
    }
}

﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class PricesResponse
    {
        public long time { get; set; }
        public List<Price> prices { get; set; }
    }
}

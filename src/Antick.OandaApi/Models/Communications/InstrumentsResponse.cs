﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class InstrumentsResponse
    {
        public List<Instrument> Instruments { get; set; }
    }
}

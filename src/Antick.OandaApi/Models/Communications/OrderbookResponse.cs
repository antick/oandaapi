﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    public class OrderbookResponse
    {
        public Dictionary<string, object> Items { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.OandaApi.Models.Communications
{
    public class CandlesResponseV20
    {
        public string Instrument { get; set; }
        public string Granularity { get; set; }
        public List<CandleV20> Candles { get; set; }
    }
}

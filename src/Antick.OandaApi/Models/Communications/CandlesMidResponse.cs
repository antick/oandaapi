﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    public class CandlesMidResponse
    {
        public string instrument { get; set; }
        public string granularity { get; set; }
        public List<CandleMid> candles { get; set; }
    }
}

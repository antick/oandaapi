﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class CandlesResponse
    {
        public string instrument  { get; set; }
        public string granularity  { get; set; }
        public List<Candle> candles { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class PositionsResponse
    {
        public List<Position> positions { get; set; }
    }
}

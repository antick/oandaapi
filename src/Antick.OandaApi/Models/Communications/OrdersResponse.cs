﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class OrdersResponse
    {
        public List<Order> orders { get; set; }
        public string nextPage { get; set; }
    }
}

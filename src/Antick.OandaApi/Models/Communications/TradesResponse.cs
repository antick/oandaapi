﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class TradesResponse
    {
        public List<TradeData> trades { get; set; }
        public string nextPage { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    public class AccountsResponse
    {
        public List<Account>  Accounts { get; set; }
    }
}

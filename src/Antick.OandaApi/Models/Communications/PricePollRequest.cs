﻿using System.Collections.Generic;

namespace Antick.OandaApi.Models.Communications
{
    class PricePollRequest
    {
        public List<string> prices;
    }
}

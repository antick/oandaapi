﻿using System.ComponentModel;

namespace Antick.OandaApi.Models
{
    public class Position
    {
        [DisplayName("Direction")]
        public string direction { get; set; }
        [DisplayName("Instrument")]
        public string instrument { get; set; }
        [DisplayName("Units")]
        public int units { get; set; }
        [DisplayName("Average Price")]
        public double avgPrice { get; set; }
    }
}

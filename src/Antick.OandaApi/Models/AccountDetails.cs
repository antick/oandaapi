﻿namespace Antick.OandaApi.Models
{
    public class AccountDetails
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public double Balance { get; set; }
        public double UnrealizedPl { get; set; }
        public double RealizedPl { get; set; }
        public double MarginUsed { get; set; }
        public double MarginAvail { get; set; }
        public int OpenTrades { get; set; }
        public int OpenOrders { get; set; }
        public double MarginRate { get; set; }
        public string AccountCurrency { get; set; }
    }
}

﻿using System;

namespace Antick.OandaApi.Models
{
    [Serializable]
    public class PricePointItem
    {
        /// <summary>
        /// Percentage short orders (отложенные ордера)
        /// </summary>
        public double os { get; set; }
        
        /// <summary>
        /// Percentage long orders (отложенные ордера)
        /// </summary>
        public double ol { get; set; }

        /// <summary>
        /// Percentage short positions (открытие позиции)
        /// </summary>
        public double ps { get; set; }

        /// <summary>
        /// Percentage long positions (открытие позиции)
        /// </summary>
        public double pl { get; set; }
        
        /// <summary>
        /// В процентах от общего обьема ордеров отложенные ордера
        /// </summary>
        public double ShortOrders
        {
            get { return os; }
            set { os = value; }
        }

        /// <summary>
        /// В процент от общего обьема орденов
        /// </summary>
        public double LongOrders
        {
            get { return ol; }
            set { ol = value; }
        }

        /// <summary>
        /// В процентах от общего обьема позиций
        /// </summary>
        public double ShortPositions
        {
            get { return ps; }
            set { ps = value; }
        }

        /// <summary>
        /// В процентах от общего обьема позиций
        /// </summary>
        public double LongPositions
        {
            get { return pl; }
            set { pl = value; }
        }

        public PricePointItem Clone()
        {
            return new PricePointItem
            {
                os = os,
                ps = ps,
                pl = pl,
                ol = ol
            };
        }
    }
}

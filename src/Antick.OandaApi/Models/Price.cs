﻿using System;

namespace Antick.OandaApi.Models
{
    public class Price
    {
        public string instrument { get; set; }
        public DateTime time;
        public double bid { get; set; }
        public double ask { get; set; }
        public double? rate { get; set; }
    }
}

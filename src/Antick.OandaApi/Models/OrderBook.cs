﻿using System;
using System.Collections.Generic;

namespace Antick.OandaApi.Models
{
    [Serializable]
    public class OrderBook
    {
        public DateTime Time { get; set; }
        public double Rate { get; set; }
        public Dictionary<double,PricePointItem> Points { get; set; }

    }
}

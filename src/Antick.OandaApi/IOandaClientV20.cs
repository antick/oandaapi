﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.OandaApi.Models;

namespace Antick.OandaApi
{
    public interface IOandaClientV20
    {

        List<CandleV20> GetCandles(string instrument, string granularity, int count = 500);
        List<CandleV20> GetCandles(string instrument, string granularity, DateTime timeFrom, int count = 500);
        List<CandleV20> GetCandles(string instrument, string granularity, DateTime timeFrom, DateTime timeTo);

        List<CandleMidV20> GetCandlesMid(string instrument, string granularity, int count = 500);
        List<CandleMidV20> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, int count = 500);
        List<CandleMidV20> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, DateTime timeTo);

    }
}

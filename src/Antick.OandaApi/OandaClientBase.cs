﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Antick.OandaApi.Configuration;
using Antick.OandaApi.Models;
using Newtonsoft.Json;

namespace Antick.OandaApi
{
    public abstract  class OandaClientBase
    {
        private readonly AccountType m_AccountType;
        private readonly string m_Token;

        protected OandaClientBase(AccountType accountType, string token)
        {
            m_AccountType = accountType;
            m_Token = token;
        }

        protected string Url()
        {
            return m_AccountType == AccountType.Demo
                ? "https://api-fxpractice.oanda.com/"
                : "https://api-fxtrade.oanda.com/";
        }

        protected T GetCandlesBase<T>(string baseUrl, Dictionary<string, object> param) where T : class
        {
            //#region instrument Validation
            //if (!param.ContainsKey("instrument"))
            //    throw new ArgumentException("instrument");

            //if (!OandaSpecs.AcceptedInstruments.Contains((string)param["instrument"]))
            //{
            //    throw new ArgumentOutOfRangeException("instrument");
            //}


//            #endregion
            #region granularity Validation
            if (param["granularity"] == null)
                param["granularity"] = "S5";

            if (param["granularity"] != null && !OandaSpecs.AcceptedTimeFrames.Contains((string)param["granularity"]))
            {
                throw new ArgumentException("granularity");
            }


            #endregion
            #region count Validation
            if (param.ContainsKey("count"))
            {
                var count = Convert.ToInt32(param["count"]);
                if (count < 0 || count > 5000)
                    throw new ArgumentOutOfRangeException("count");
            }


            #endregion

            //#region candleFormat Validation
            //if (!param.ContainsKey("candleFormat"))
            //{
            //    // Значение по умолчанию
            //    param["candleFormat"] = "bidask";
            //}
            //if (!OandaSpecs.AcceptedCandleFormat.Contains((string)param["candleFormat"]))
            //    throw new ArgumentOutOfRangeException("candleFormat");


            //#endregion
            
            #region includeFirst Validation
            if (param.ContainsKey("from"))
            {
                if (!param.ContainsKey("includeFirst"))
                    param["includeFirst"] = "true";

                var bTmp = false;
                if (!(param["includeFirst"] == "true" || param["includeFirst"] == "false"))
                    throw new ArgumentOutOfRangeException("includeFirst");
            }
            #endregion

            //TODO
            // start validation
            // end validation
            // dailyAlignment validation
            // alignmentTimezone validation
            // weeklyAlignment validation

            string requestSubStr = string.Empty;
            var i = 0;
            foreach (var o in param)
            {
                if (i == 0) requestSubStr += "?";
                else requestSubStr += "&";
                requestSubStr += o.Key + "=" + o.Value;
                i++;
            }

            string requestString = baseUrl + requestSubStr;
            string responseString = MakeRequest(requestString);
            var candlesResponse = JsonConvert.DeserializeObject<T>(responseString);
            return candlesResponse;
        }

        ///  <summary>
        ///  send a request and retrieve the response
        ///  </summary>
        ///  <param name="requestString">Запрос, без части сервера. 
        /// <remarks>requestString примерный формат "labs/v1/orderbook_data?instrument=EUR_USD&period=3600"</remarks></param>
        /// <param name="postData"></param>
        /// <param name="method"></param>
        /// <returns>the response string</returns>
        protected string MakeRequest(string requestString, string method = "GET", string postData = null)
        {
            string requestStr = Url() + requestString;

            var request = WebRequest.CreateHttp(requestStr);
            request.Headers.Add("Authorization", "Bearer " + m_Token);
            request.Headers.Add("Accept-Datetime-Format", "RFC3339");
            request.Method = method;

            if (method == "POST" && !string.IsNullOrEmpty(postData))
            {
                var data = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string responseString = reader.ReadToEnd().Trim();
                        return responseString;
                    }
                }
            }
            catch (WebException ex)
            {
                throw;
            }
        }
    }
}

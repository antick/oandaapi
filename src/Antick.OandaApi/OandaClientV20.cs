﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.OandaApi.Configuration;
using Antick.OandaApi.Models;
using Antick.OandaApi.Models.Communications;
using Antick.OandaApi.Utils;

namespace Antick.OandaApi
{
    public class OandaClientV20 : OandaClientBase, IOandaClientV20
    {
        public OandaClientV20(AccountType accountType, string token):base(accountType, token)
        {
        }

        public List<CandleV20> GetCandles(string instrument, string granularity, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandlesResponseV20>($"v3/instruments/{instrument}/candles", new Dictionary<string, object>
                {
                    {"granularity", granularity},
                    {"count", count },
                    {"price", "BA" }
                });

            return candlesResponse.Candles;
        }

        public List<CandleV20> GetCandles(string instrument, string granularity, DateTime timeFrom, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandlesResponseV20>($"v3/instruments/{instrument}/candles", new Dictionary<string, object>
                {
                    {"granularity", granularity},
                    {"start", OandaUtils.TimeToRest(timeFrom) },
                    {"count", count },
                    {"price", "BA" }
                });
            return candlesResponse.Candles;
        }

        public List<CandleV20> GetCandles(string instrument, string granularity, DateTime timeFrom, DateTime timeTo)
        {
            var candlesResponse =
                GetCandlesBase<CandlesResponseV20>($"v3/instruments/{instrument}/candles", new Dictionary<string, object>
                {
                    {"granularity", granularity},
                    {"start", OandaUtils.TimeToRest(timeFrom)},
                    {"end", OandaUtils.TimeToRest(timeTo)},
                    {"price", "BA" }
                });
            return candlesResponse.Candles;
        }

        public List<CandleMidV20> GetCandlesMid(string instrument, string granularity, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandleMidResponseV20>($"v3/instruments/{instrument}/candles", new Dictionary<string, object>
                {
                    {"granularity", granularity},
                    {"count", count },
                    {"price", "M" }
                });

            return candlesResponse.Candles;
        }

        public List<CandleMidV20> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandleMidResponseV20>($"v3/instruments/{instrument}/candles", new Dictionary<string, object>
                {
                    {"granularity", granularity},
                    {"from", OandaUtils.TimeToRest(timeFrom) },
                    {"count", count },
                    {"price", "M" }
                });
            return candlesResponse.Candles;
        }

        public List<CandleMidV20> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, DateTime timeTo)
        {
            var candlesResponse =
                GetCandlesBase<CandleMidResponseV20>($"v3/instruments/{instrument}/candles", new Dictionary<string, object>
                {
                    {"granularity", granularity},
                    {"from", OandaUtils.TimeToRest(timeFrom)},
                    {"to", OandaUtils.TimeToRest(timeTo)},
                    {"price", "M" }
                });
            return candlesResponse.Candles;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.OandaApi.Models;

namespace Antick.OandaApi
{
    public interface IOandaClient
    {
        List<Account> GetAccountsList();
        AccountDetails GetAccountDetails(int accountId);
        
        List<Instrument> GetInstruments(int accountId);

        List<TradeData> GetTradeList(int account);

        List<Order> GetOrderList(int account);

        int GetMostRecentTransactionId(int account);
        List<Transaction> GetTransactionList(int account, int minTransId);

        List<Position> GetPositions(int account);

        List<Candle> GetCandles(string instrument, string granularity, int count = 500);
        List<Candle> GetCandles(string instrument, string granularity, DateTime timeFrom, int count = 500);
        List<Candle> GetCandles(string instrument, string granularity, DateTime timeFrom, DateTime timeTo);

        List<CandleMid> GetCandlesMid(string instrument, string granularity, int count = 500);
        List<CandleMid> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, int count = 500);
        List<CandleMid> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, DateTime timeTo);
   
        void PostMarketOrder(int account, Dictionary<string, string> requestParams);

        List<Price> GetRates(List<Instrument> instruments);

        string GetOrderbookDataStr(string instrument, string period);
        List<OrderBook> GetOrderbookData(string instrument, string period);
    }
}

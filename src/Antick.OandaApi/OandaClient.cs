﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using Antick.OandaApi.Configuration;
using Antick.OandaApi.Models;
using Antick.OandaApi.Models.Communications;
using Antick.OandaApi.Utils;
using Newtonsoft.Json;
using static System.Boolean;

namespace Antick.OandaApi
{
    /// <summary>
    /// Rest API до Оанды
    /// </summary>
    public class OandaClient : OandaClientBase, IOandaClient
    {
        public OandaClient(AccountType accountType, string token):base(accountType, token)
        {
        }
        
        /// <summary>
        /// Список аккаунтов
        /// </summary>
        public List<Account> GetAccountsList()
        {
            const string request = "v1/accounts/";
            string responseString = MakeRequest(request);
            var response = JsonConvert.DeserializeObject<AccountsResponse>(responseString);
            return response.Accounts;
        }

        /// <summary>
        /// Детальная информация об аккаунте
        /// </summary>
        public AccountDetails GetAccountDetails(int accountId)
        {
            string requestString = "v1/accounts/" + accountId;
            string responseString = MakeRequest(requestString);
            var accountDetails = JsonConvert.DeserializeObject<AccountDetails>(responseString);
            return accountDetails;
        }
        
        /// <summary>
        /// Список торговых символом у Оанды
        /// </summary>
        public  List<Instrument> GetInstruments(int accountId)
        {
            string requestString = "v1/instruments?accountId=" + accountId;
            string responseString = MakeRequest(requestString);
            var instrumentResponse = JsonConvert.DeserializeObject<InstrumentsResponse>(responseString);
            return instrumentResponse.Instruments;
        }

        /// <summary>
        /// Gets the list of open trades for a given account
        /// </summary>
        /// <param name="account">the account ID of the account</param>
        /// <returns>list of open trades (empty list if there are none)</returns>
        public List<TradeData> GetTradeList(int account)
        {
            string requestString = "v1/accounts/" + account + "/trades";
            string responseString = MakeRequest(requestString);
            
            TradesResponse tradeResponse = JsonConvert.DeserializeObject<TradesResponse>(responseString);
            List<TradeData> trades = new List<TradeData>();

            trades.AddRange(tradeResponse.trades);
            // TODO: should loop through the "next pages"

            return trades;
        }

        /// <summary>
        /// Get the list of open orders for a given account
        /// </summary>
        /// <param name="account">the account ID of the account</param>
        /// <returns>list of open orders (empty list if there are none)</returns>
        public List<Order> GetOrderList(int account)
        {
            string requestString = "v1/accounts/" + account + "/orders";
            string responseString = MakeRequest(requestString);
            
            OrdersResponse tradeResponse = JsonConvert.DeserializeObject<OrdersResponse>(responseString);

            List<Order> orders = new List<Order>();
            orders.AddRange(tradeResponse.orders);
            // TODO: should loop through the "next pages"

            return orders;
        }

        /// <summary>
        /// Gets the most recent transaction ID for the account specified
        /// </summary>
        /// <param name="account">the account ID of the account to check</param>
        /// <returns>the most recent transaction ID or -1 if no transactions are found</returns>
        public int GetMostRecentTransactionId(int account)
        {
            int result = -1;
            string requestString = "v1/accounts/" + account + "/transactions?maxCount=1";
            string responseString = MakeRequest(requestString);
            
            var tradeResponse = JsonConvert.DeserializeObject<TransactionsResponse>(responseString);
            if (tradeResponse.transactions.Count > 0)
            {
                result = tradeResponse.transactions[0].id;
            }

            return result;
        }

        /// <summary>
        /// retrieves a list of transactions in descending order
        /// https://github.com/oanda/apidocs/blob/master/sections/transactions.md
        /// </summary>
        /// <param name="account">the id of the account to load transactions for</param>
        /// <param name="minTransId">retrieve all transactions that are more recent (larger ID) than this id</param>
        /// <returns>the list of transactions (empty list if none)</returns>
        public List<Transaction> GetTransactionList(int account, int minTransId)
        {
            string requestString = "v1/accounts/" + account + "/transactions?minId=" + minTransId;

            string responseString = MakeRequest(requestString);

           
            var transactions = new List<Transaction>();
            var dataResponse = JsonConvert.DeserializeObject<TransactionsResponse>(responseString);
            transactions.AddRange(dataResponse.transactions);
            // TODO: should loop through the "next pages"

            return transactions;
        }

        

        /// <summary>
        /// Get the current open positions for the account specified
        /// </summary>
        /// <param name="account">the ID of the account</param>
        /// <returns>list of positions (or empty list if there are none)</returns>
        public List<Position> GetPositions(int account)
        {
            string requestString = "v1/accounts/" + account + "/positions";

            string responseString = MakeRequest(requestString);

            var positionResponse = JsonConvert.DeserializeObject<PositionsResponse>(responseString);
            var positions = new List<Position>();
            positions.AddRange(positionResponse.positions);

            return positions;
        }

       

      

        /// <summary>
        /// Получить бары в рамках контекса клиента
        /// </summary>
        public List<Candle> GetCandles(string instrument, string granularity, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandlesResponse>("v1/candles", new Dictionary<string, object>
                {
                    {"instrument", instrument},
                    {"granularity", granularity},
                    {"count", count }
                });
            return candlesResponse.candles;
        }


        public List<Candle> GetCandles(string instrument, string granularity, DateTime timeFrom, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandlesResponse>("v1/candles", new Dictionary<string, object>
                {
                    {"instrument", instrument},
                    {"granularity", granularity},
                    {"start", OandaUtils.TimeToRest(timeFrom) },
                    {"count", count }
                });
            return candlesResponse.candles;
        }

        public List<CandleMid> GetCandlesMid(string instrument, string granularity, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandlesMidResponse>("v1/candles", new Dictionary<string, object>
                {
                    {"instrument", instrument},
                    {"granularity", granularity},
                    {"candleFormat", "midpoint" },
                    {"count", count }
                });
            return candlesResponse.candles;
        }

        /// <summary>
        /// Получить бары в рамках контекса клиента
        /// </summary>
        public List<CandleMid> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, int count = 500)
        {
            var candlesResponse =
                GetCandlesBase<CandlesMidResponse>("v1/candles", new Dictionary<string, object>
                {
                    {"instrument", instrument},
                    {"granularity", granularity},
                    {"candleFormat", "midpoint"},
                    {"start", OandaUtils.TimeToRest(timeFrom)},
                    {"count", count}
                });
            return candlesResponse.candles;
        }

        public List<CandleMid> GetCandlesMid(string instrument, string granularity, DateTime timeFrom, DateTime timeTo)
        {
            var candlesResponse =
                GetCandlesBase<CandlesMidResponse>("v1/candles", new Dictionary<string, object>
                {
                    {"instrument", instrument},
                    {"granularity", granularity},
                    {"candleFormat", "midpoint"},
                    {"start", OandaUtils.TimeToRest(timeFrom)},
                    {"end", OandaUtils.TimeToRest(timeTo)}
                });
            return candlesResponse.candles;
        }

        public List<Candle> GetCandles(string instrument, string granularity, DateTime timeFrom, DateTime timeTo)
        {
            var candlesResponse =
                GetCandlesBase<CandlesResponse>("v1/candles", new Dictionary<string, object>
                {
                    {"instrument", instrument},
                    {"granularity", granularity},
                    {"start", OandaUtils.TimeToRest(timeFrom)},
                    {"end", OandaUtils.TimeToRest(timeTo)}
                });
            return candlesResponse.candles;
        }

        /// <summary>
        /// Execute a marketOrder on the given account using the given parameters
        /// </summary>
        /// <param name="account">the ID of the account to use</param>
        /// <param name="requestParams">dictionary of parameters for the request key=Name, value=Value</param>
        public void PostMarketOrder(int account, Dictionary<string, string> requestParams)
        {
            string requestString = "v1/accounts/" + account + "/orders";

            var postData = "";
            foreach (var pair in requestParams)
            {
                postData += pair.Key + "=" + pair.Value + "&";
            }
            postData += "type=market";

            string responseString = MakeRequest(requestString, "POST", postData);
            // TODO: make use of the response
        }

     

        /// <summary>
        /// Gets the current rates for the given instruments
        /// </summary>
        /// <param name="instruments">The list of instruments to request</param>
        /// <returns>The list of prices</returns>
        public List<Price> GetRates(List<Instrument> instruments)
        {
            var requestBuilder = new StringBuilder("v1/prices?instruments=");

            foreach (var instrument in instruments)
            {
                requestBuilder.Append(instrument.instrument + ",");
            }
            // Grab the string and remove the trailing comma
            string requestString = requestBuilder.ToString().Trim(',');

            string responseString = MakeRequest(requestString);

            var serializer = new JavaScriptSerializer();
            var pricesResponse = serializer.Deserialize<PricesResponse>(responseString);
            List<Price> prices = new List<Price>();
            prices.AddRange(pricesResponse.prices);

            return prices;
        }

        public string GetOrderbookDataStr(string instrument, string period)
        {
            var requestBuilder = new StringBuilder(string.Format(
                "labs/v1/orderbook_data?instrument={0}&period={1}", instrument, period));

            // Grab the string and remove the trailing comma
            string requestString = requestBuilder.ToString();

            string responseString = MakeRequest(requestString);
            return responseString;
        }

        public List<OrderBook> GetOrderbookData(string instrument, string period)
        {
            string responseString = GetOrderbookDataStr(instrument, period);

            var serializer = new JavaScriptSerializer();
            var pricesResponse = serializer.DeserializeObject(responseString);

            var root = pricesResponse as Dictionary<string,object>;
            var res = new List<OrderBook>();
            foreach (var i in root)
            {
                var time = i.Key;
                var obj = i.Value as Dictionary<string,object>;

                var orderItem = new OrderBook
                {
                    Time = DateTime.SpecifyKind(OandaUtils.UnixDateTimeToDateTime(Convert.ToInt32(time)),DateTimeKind.Utc).ToLocalTime(),
                    Rate = Convert.ToDouble(obj["rate"]),
                    Points = new Dictionary<double, PricePointItem>()
                };

                var pPoints = obj["price_points"] as Dictionary<string, object>;
                foreach (var pPoint in pPoints)
                {
                    var pPointValue = pPoint.Value as Dictionary<string,object>;
                    var pricePointItem = new PricePointItem
                    {
                        ol = Convert.ToDouble(pPointValue["ol"]),
                        os = Convert.ToDouble(pPointValue["os"]),
                        pl = Convert.ToDouble(pPointValue["pl"]),
                        ps = Convert.ToDouble(pPointValue["ps"])
                    };
                    orderItem.Points.Add(Convert.ToDouble(pPoint.Key,new NumberFormatInfo{CurrencyDecimalSeparator = "."}), pricePointItem);
                }

                res.Add(orderItem);
                res = res.OrderByDescending(p => p.Time).ToList();
            }
            return res;
        }

       
    }
}


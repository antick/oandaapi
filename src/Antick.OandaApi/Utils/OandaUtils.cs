﻿using System;
using System.Web;

namespace Antick.OandaApi.Utils
{
    public class OandaUtils
    {
        /// <summary>
        /// Форматирование даты для посылки в Оанду
        /// </summary>
        public static string TimeToRest(DateTime time)
        {
            return HttpUtility.UrlEncode(time.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"));
        }

        public static DateTime UnixDateTimeToDateTime(int mt4Time)
        {
            var Out = new DateTime(1970, 1, 1, 0, 0, 0);
            return Out.AddSeconds(mt4Time);
        }
    }
}
